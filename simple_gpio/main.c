/* Simple LED task demo using semaphore, queue and timed delays:
 *
 * The LED on PC13 is toggled in task1 every second.
 */

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include <task.h>
#include <queue.h>
#include <semphr.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#define LED_PORT GPIOC
#define LED_PIN	 GPIO13
#define MS_DELAY 1000

QueueHandle_t led_queue_hdl;
xSemaphoreHandle swtimer_semaphore_hdl;

extern void vApplicationStackOverflowHook(
	xTaskHandle *pxTask,
	signed portCHAR *pcTaskName);

void
vApplicationStackOverflowHook(
  xTaskHandle *pxTask __attribute((unused)),
  signed portCHAR *pcTaskName __attribute((unused))
) {
	for(;;);	// Loop forever here..
}

static void
led_task(void *args __attribute((unused))) {
	BaseType_t xStatus;
	char message;
	for (;;) {
		xStatus = xQueueReceive( led_queue_hdl, &message, 500);
		if(xStatus == pdPASS){
			gpio_toggle(LED_PORT,LED_PIN);
		}
	}
}

static void
timer_handle_task(void *args __attribute((unused))){
	char message;
	BaseType_t xSemStatus;
	for(;;){
		xSemStatus = xSemaphoreTake(swtimer_semaphore_hdl,500);
		if(xSemStatus == pdPASS){
			message = '1';
			xQueueSend(led_queue_hdl, &message, 500);
		}
	}
}

static void
swtimer_sem_task(void *args __attribute((unused))){
	BaseType_t xSemStatus;
	for(;;){
		vTaskDelay(pdMS_TO_TICKS(MS_DELAY));
		xSemStatus = xSemaphoreGive(swtimer_semaphore_hdl);
	}
}


int
main(void) {

	rcc_clock_setup_in_hse_8mhz_out_72mhz(); // For "blue pill"

	led_queue_hdl = xQueueCreate( 5, sizeof(char));
	swtimer_semaphore_hdl = xSemaphoreCreateBinary();

	//Button side
//    rcc_periph_clock_enable(RCC_GPIOB);
//	gpio_set_mode(
//		BUTTON_PORT,
//		GPIO_MODE_INPUT,
//		GPIO_CNF_INPUT_PULL_UPDOWN,
//		BUTTON_PIN);
//	gpio_set(BUTTON_PORT,BUTTON_PIN);





	//LED side
	rcc_periph_clock_enable(RCC_GPIOC);
	gpio_set_mode(
		LED_PORT,
		GPIO_MODE_OUTPUT_2_MHZ,
		GPIO_CNF_OUTPUT_PUSHPULL,
		LED_PIN);

	xTaskCreate(led_task,"LEDTASK",100,NULL,configMAX_PRIORITIES-1,NULL);
	xTaskCreate(timer_handle_task,"TIMERHANDLETASK",100,NULL,configMAX_PRIORITIES-1,NULL);
	xTaskCreate(swtimer_sem_task, "SWTIMERSEMTASK",100,NULL,configMAX_PRIORITIES-1,NULL);
	vTaskStartScheduler();

	for (;;);
	return 0;
}


// End
